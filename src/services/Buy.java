package services;

import models.Client;
import models.Product;
import models.Supplier;
import models.TaxCoupon;

import java.util.List;

public class Buy {

    public void generateCouponToBuy(Client client, Supplier supplier, List<Product> productList){
        double totalPrice = calculateTotalPrice(productList);
        decreasesInventoryOfProducts(productList);

        TaxCoupon taxCoupon = new TaxCoupon(totalPrice, client, supplier);

        System.out.println("Cupom fiscal gerado: " + taxCoupon.toString());
    }

    public double calculateTotalPrice(List<Product> productList){
        double total = 0;

        for(Product p : productList){
            total += p.getPriceUnit();
        }

        return total;
    }

    public void decreasesInventoryOfProducts(List<Product> productList){
        for(Product p : productList){
            p.decreasesInventory();;
        }
    }

}
