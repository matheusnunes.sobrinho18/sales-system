package models;

public class TaxCoupon {

    private Double totalPrice;
    private Client client;
    private Supplier supplier;

    public TaxCoupon(Double totalPrice, Client client, Supplier supplier) {
        this.totalPrice = totalPrice;
        this.client = client;
        this.supplier = supplier;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
    }
}
