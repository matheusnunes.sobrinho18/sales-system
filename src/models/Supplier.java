package models;

import java.util.List;

public class Supplier {

    private String name;
    private List<Product> offeredProductList;

    public Supplier(String name, List<Product> productlist) {
        this.name = name;
        this.offeredProductList = productlist;
    }

    public Supplier(String name) {
        this.name = name;
    }

    public void addProductList(Product product){
        this.offeredProductList.add(product);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProductList() {
        return offeredProductList;
    }

    public void setProductList(List<Product> productList) {
        this.offeredProductList = productList;
    }
}
