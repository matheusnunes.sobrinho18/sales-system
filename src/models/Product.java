package models;

public class Product {

    private String name;
    private String description;
    private Double priceUnit;

    public Product(String name, String description, Double priceUnit) {
        this.name = name;
        this.description = description;
        this.priceUnit = priceUnit;
    }

    public Product(String name, Double priceUnit) {
        this.name = name;
        this.priceUnit = priceUnit;
    }

    public void decreasesInventory(){
        System.out.println("Indo no banco e decrementando o estoque do produto em questão");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPriceUnit() {
        return priceUnit;
    }

    public void setPriceUnit(Double priceUnit) {
        this.priceUnit = priceUnit;
    }
}
