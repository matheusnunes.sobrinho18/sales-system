Sistema de vendas

Existem 4 entidades dentro da pasta models:

- Client: são os clientes 
- Product: são os produtos
- Supplier: são os fornecedores, nela irá ter uma lista de produtos ofertados por esse fornecedor
- TaxCoupon: é o cupom fiscal que irá ser gerado pela classe de serviço Buy, nela só preciso ter o cliente que efetuou a compra, o fornecedor da operação e preço total

E tenho também uma única classe de service:

- Buy: nela é gerado o cupom fiscal, calculado o preço total da compra e também é onde chamamos o método para diminuir o estoque dos produtos comprados.